import cv2


def ResizeWithAspectRatio(resize_image, width=None, height=None, inter=cv2.INTER_AREA):
    dim = None
    (h, w) = resize_image.shape[:2]

    if width is None and height is None:
        return resize_image
    if width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    else:
        r = width / float(w)
        dim = (width, int(h * r))

    return cv2.resize(resize_image, dim, interpolation=inter)


def show(image, text=""):
    if image.shape[0] < 1000:
        cv2.imshow(text, image)
    else:
        # capture = capture.astype("uint8")
        cv2.imshow(text, ResizeWithAspectRatio(image, width=1200))
    cv2.waitKey()
