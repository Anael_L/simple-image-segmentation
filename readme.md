# Quick image segmentation program

This program enables quick image segmentation by thresholding multiple channels. For the program to work, please add images to be processed as follows:

```text
- repo parent directory
  ├── repo root
  └── data
```

After the program runs, the filesystem will look like this:

```text
- repo parent directory
  ├── repo root
  ├── data
  └── masks
```

## Usage recommendation

The recommended usage is for segmentation of textureless objects with a background of contrastive color. To run the program, use the following command:

```bash
python filter_app.py
```

## How it works

### Color spaces

Different color spaces can be used to visualize and manipulate images in a way that is more "natural" to human perception. Two examples of such color spaces are the HSV color space and the LAB color space.

<div style="display: flex; align-items: center;">
  <img src="media/hsv.jpg" alt="HSV Colorspace" height="200"/>
  <span>&nbsp;&nbsp;&nbsp;&nbsp;HSV Colorspace&nbsp;&nbsp;&nbsp;&nbsp;</span>
  <img src="media/lab.jpg" alt="LAB Colorspace" height="200"/>
  <span>&nbsp;&nbsp;&nbsp;&nbsp;LAB Colorspace&nbsp;&nbsp;&nbsp;&nbsp;</span>
</div>

Converting an image to either of those formats enables for segmentation based on color only.

### Algorithm

<figure>
  <img src="media/algorithm.jpg" alt="Algorithm">
  <figcaption>Simple mask thresholding algorithm</figcaption>
</figure>

## Results

<div style="display: flex;flex-direction: column;">
  <div style="flex: 50%; padding: 10px;">
    <figure>
      <img src="media/segmentation_1.jpg" alt="Plant segmentation example" style="max-width: 100%;">
      <figcaption>Potato (?) segmentation</figcaption>
    </figure>
  </div>
  <div style="flex: 50%; padding: 10px;">
    <figure>
      <img src="media/segmentation_2.jpg" alt="Plant segmentation example" style="max-width: 100%;">
      <figcaption>Corn (?) segmentation</figcaption>
    </figure>
  </div>
</div>

Here, we can see that corn and potatoes at an advanced growth stages can be easily segmented by tuning a single parameter. Similar results are obtained using either LAB or HSV color spaces. The algorithm has O(n) complexity and effectively runs in real-time. As long as the segmented objects and background do not share pixel values, the segmentation quality will be optimal.

## Challenges

Some images can be much more challenging and need careful tuning of multiple parameters, depending on texture complexity. Therefore, some images can not be perfectly segmented using pixel color alone, when some pixels belonging to our target objects are identical to some background pixels.

## Improvement options

To further refine those segmentations, it is possible to apply simple filtering techniques such as:

* opening (erosion followed by a dilation) : allows to remove small objects from the background. This can filter out a small number of background pixels with color matching the color range of our target object
* closing (dilation followed by an erosion) : Allows to fill in holes in the segmentation masks when too many pixels were removed.

The proposed software is of manual use but similar images can be effectively filtered with identical parameters, making the segmenting process seamless for small dataset creation.

Several methods can be used to generate further segmented data, provided the diversity of the already segmented data matches the diversity of the new data:

* Optimal filter parameters could be saved, in addition to segmentation masks. When a new unlabeled image is added to the dataset, one could try to find the most similar unlabeled image and apply the same filtering parameters. A similarity metric could be the mean square distance between the histogram of values for a collection of appropriate channels, in an appropriate color space.
* The program could also make use of a simple heuristic to make the initial manual filtering easier. One suggestion could be to use the convert_rgb_to_names method from vision.py to determine pixels that correspond to a plant color. The algorithm could suggest initial channel thresholds that rule out colors that are not expected to be target color. Depending on the setting of the image capture, though (weather, time of day, ...) it can prove challenging to suggest the correct thresholds. Also, this requires careful selection of accepted colors. This is still possible when the number of colors encountered in images is limited, which is the case for crops images.

## Conclusion

This program is a good compromise for fast segmentation dataset creation based on pixel color without sacrificing quality, limited to small size datasets. The segmented images can be further refined and later be leveraged to label even more images with no human interaction required.
