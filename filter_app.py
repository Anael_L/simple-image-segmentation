import cv2
import tkinter as tk
from tkinter import filedialog, ttk
import os
from pathlib import Path
from PIL import Image, ImageTk
from vision import ResizeWithAspectRatio
import numpy as np
from functools import partial


def hsv_thresholding(h_range, s_range, v_range, image):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    h_mask = np.logical_and(
        hsv[:, :, 2] >= h_range.low,
        hsv[:, :, 2] <= h_range.high,
    )
    s_mask = np.logical_and(
        hsv[:, :, 1] >= s_range.low,
        hsv[:, :, 1] <= s_range.high,
    )
    v_mask = np.logical_and(
        hsv[:, :, 0] >= v_range.low,
        hsv[:, :, 0] <= v_range.high,
    )
    return h_mask * s_mask * v_mask


def lab_thresholding(lightness, green_minus_red, blue_minus_yellow, image):
    lab = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
    lightness_mask = np.logical_and(
        lab[:, :, 0] >= lightness.low,
        lab[:, :, 0] <= lightness.high,
    )
    blue_minus_yellow_mask = np.logical_and(
        lab[:, :, 1] >= blue_minus_yellow.low,
        lab[:, :, 1] <= blue_minus_yellow.high,
    )
    green_minus_red_mask = np.logical_and(
        lab[:, :, 2] >= green_minus_red.low,
        lab[:, :, 2] <= green_minus_red.high,
    )
    return lightness_mask * green_minus_red_mask * blue_minus_yellow_mask


def image_generator(root):
    image_files = os.listdir(root)
    for image_file in image_files:
        yield root / image_file


def filter_image(image, masking_function):
    mask = masking_function(image)
    filtered_image = np.zeros_like(image)
    filtered_image[mask] = image[mask]
    return filtered_image


class thresh_range:
    def __init__(self, low, high, name):
        self.low = low
        self.high = high
        self.name = name

    def update(self, thresh_type, new_value):
        if thresh_type == "low":
            self.low = int(new_value)
        else:
            self.high = int(new_value)
        return (self.low, self.high)

    def gen_scale_interface(self, root, command):
        # Create a frame for sliders
        slider_frame = ttk.Frame(root)
        slider_frame.pack(padx=10, pady=10)
        # Create a slider for HSV values
        scale_low = tk.Scale(
            slider_frame,
            from_=self.low,
            to=self.high,
            orient=tk.HORIZONTAL,
            label=f"{self.name} low thresh",
            length=200,
            sliderlength=20,
            command=partial(command, "low"),
        )
        scale_high = tk.Scale(
            slider_frame,
            from_=self.low,
            to=self.high,
            orient=tk.HORIZONTAL,
            label=f"{self.name} high thresh",
            length=200,
            sliderlength=20,
            command=partial(command, "high"),
        )
        scale_low.pack()
        scale_high.pack()
        scale_high.set(self.high)
        return slider_frame


class app:
    def __init__(self, masking_ranges, masking_function):
        self.root = tk.Tk()
        self.root.title("Image Filter by LAB")
        self.masking_function = masking_function
        self.masking_ranges = masking_ranges
        # Create a frame for sliders
        sliders_frame = ttk.Frame(self.root)
        sliders_frame.pack(padx=10, pady=10, fill=tk.X)
        for masking_range in self.masking_ranges:
            slider_frame = masking_range.gen_scale_interface(
                root=sliders_frame,
                command=partial(self.range_update, masking_range.name),
            )
            slider_frame.pack(side="left")

        cur_dir = Path(__file__).parent.resolve()
        data_dir = cur_dir.parent / "data"
        self.generator = image_generator(data_dir)

        # Create buttons
        buttons_frame = ttk.Frame(self.root)
        buttons_frame.pack(padx=10, pady=10)

        next_button = ttk.Button(
            buttons_frame, text="Next image", command=self.next_image
        )
        next_button.pack(padx=5, side="left")

        save_button = ttk.Button(
            buttons_frame, text="Save mask", command=self.save_mask
        )
        save_button.pack(padx=5, side="left")

        quit_button = ttk.Button(buttons_frame, text="Quit", command=self.quit_app)
        quit_button.pack(padx=5, side="left")

        self.mask_exist_status = ttk.Label(buttons_frame, text="no existing mask")
        self.mask_exist_status.pack(padx=5, side="left")

        self.panel = tk.Label(self.root, image=None)
        self.next_image()
        self.panel.pack(side="bottom", fill="both", expand="yes")

    def range_update(self, range_name, range_position, range_value):
        for mask_range in self.masking_ranges:
            if mask_range.name == range_name:
                mask_range.update(range_position, range_value)
                break
        self.filter_image()

    def filter_image(self):
        display_image = filter_image(
            self.display_image, partial(self.masking_function, *self.masking_ranges)
        )
        self.show_image(display_image)

    def mask_exists(self):
        save_mask_path = self.raw_image_path.parent.parent / "masks"
        mask_name = str(self.raw_image_path.stem)
        mask_suffix = str(self.raw_image_path.suffix)
        return os.path.isfile(str(save_mask_path / (mask_name + mask_suffix)))

    def update_mask_exists_status(self):
        if self.mask_exists():
            self.mask_exist_status.config(text="mask exists!")
        else:
            self.mask_exist_status.config(text="no existing mask")

    def save_mask(self):
        mask = self.masking_function(*self.masking_ranges, self.raw_image)
        save_mask_path = self.raw_image_path.parent.parent / "masks"
        os.makedirs(save_mask_path, exist_ok=True)
        mask_name = str(self.raw_image_path.stem)
        mask_suffix = str(self.raw_image_path.suffix)

        cv2.imwrite(
            str(save_mask_path / (mask_name + mask_suffix)),
            mask * 255,
        )
        self.update_mask_exists_status()

    def loop(self):
        self.root.mainloop()

    def quit_app(self):
        self.root.quit()
        self.root.destroy()

    def show_image(self, image):
        tk_img = ImageTk.PhotoImage(image=Image.fromarray(image))
        self.panel.configure(image=tk_img)
        self.panel.image = tk_img

    def next_image(self):
        self.raw_image_path = self.generator.__next__()
        self.raw_image = cv2.imread(str(self.raw_image_path))
        self.display_image = ResizeWithAspectRatio(self.raw_image, height=600)
        self.filter_image()
        self.update_mask_exists_status()


if __name__ == "__main__":
    # range_element_hue = thresh_range(0, 255, "hue")
    # range_element_saturation = thresh_range(0, 255, "saturation")
    # range_element_value = thresh_range(0, 255, "value")
    # masking_ranges = [range_element_hue, range_element_saturation, range_element_value]
    # masking_function = hsv_thresholding
    # filtering_app = app(masking_ranges, masking_function)

    range_l = thresh_range(0, 255, "lightness")
    range_a = thresh_range(0, 255, "green_minus_red")
    range_b = thresh_range(0, 255, "blue_minus_yellow")
    masking_ranges = [range_l, range_a, range_b]
    masking_function = lab_thresholding
    filtering_app = app(masking_ranges, masking_function)
    filtering_app.loop()
