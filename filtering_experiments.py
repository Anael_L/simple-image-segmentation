from vision import *
from webcolors import rgb_to_name
from scipy.spatial import KDTree
from webcolors import (
    CSS3_HEX_TO_NAMES,
    hex_to_rgb,
)
from sklearn.decomposition import PCA
import os
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt


def pixel_to_color_name(pixel):
    try:
        return rgb_to_name(tuple(pixel))
    except:
        return "None"


def convert_rgb_to_names(rgb_tuple):
    # a dictionary of all the hex and their respective names in css3
    css3_db = CSS3_HEX_TO_NAMES
    names = []
    rgb_values = []
    for color_hex, color_name in css3_db.items():
        names.append(color_name)
        rgb_values.append(hex_to_rgb(color_hex))

    kdt_db = KDTree(rgb_values)
    distance, index = kdt_db.query(rgb_tuple)
    return names[index]


def is_color_plant(color_str):
    print(color_str)
    return "gray" not in color_str


def fast_naive_green_filter(image, hue_range):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    h_mask = np.logical_and(hsv[:, :, 2] > hue_range[0], hsv[:, :, 2] < hue_range[1])
    s_mask = hsv[:, :, 2] < np.multiply(hsv[:, :, 1], 2)
    v_mask = np.logical_and(hsv[:, :, 2] > 4, hsv[:, :, 2] < 120)
    print(f"fraction of accepted hues:\n{np.sum(h_mask)/np.product(h_mask.shape)}")
    return h_mask * s_mask * v_mask


def bgr_to_g_minus_r(image):
    return (image[:, :, 1] - image[:, :, 2]).astype(np.uint8)


def bgr_to_b_minus_r(image):
    return (image[:, :, 0] - image[:, :, 2]).astype(np.uint8)


def image_pca(image):
    # Flatten the 12-channel image into a 2D array
    height, width, channels = image.shape
    flattened_image = image.reshape(height * width, channels)

    # Perform PCA on the flattened image
    n_components = 1
    pca = PCA(n_components=n_components)
    transformed_image = pca.fit_transform(flattened_image)

    # Normalize the single channel image to 0-255 range
    transformed_image = cv2.normalize(transformed_image, None, 0, 255, cv2.NORM_MINMAX)

    # Convert the single channel image to 8-bit image format
    transformed_image = np.uint8(transformed_image).reshape(height, width)

    # Display the single channel summarized image using OpenCV
    return transformed_image


if __name__ == "__main__":
    cur_dir = Path(__file__).parent.resolve()
    data_dir = cur_dir.parent / "data"
    image_files = os.listdir(data_dir)
    sample_size = 100
    for image_file in image_files:
        # image_file = "crop_test.jpg"
        image = cv2.imread(str(data_dir / image_file))
        # features = [
        #     image,
        #     np.expand_dims(bgr_to_b_minus_r(image), axis=-1),
        #     np.expand_dims(bgr_to_g_minus_r(image), axis=-1),
        #     cv2.cvtColor(image, cv2.COLOR_BGR2HSV),
        # ]
        # features = np.concatenate(features, axis=2)
        # transformed_image = image_pca(features)
        # show(transformed_image)
        # counts, bins = np.histogram(transformed_image.flatten())
        # plt.stairs(counts, bins)
        # plt.show(block=True)
        # continue

        # rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        # Filter by hue
        show(image, text="raw image")
        filtered_image = np.zeros_like(image)
        mask = fast_naive_green_filter(image)
        mask2 = fast_naive_green_filter_2(image)
        # combined_masks = mask * mask2
        # combined_masks = np.logical_and(mask, mask2)
        # filtered_image[combined_masks] = image[combined_masks]

        filtered_image[mask] = image[mask]
        # filtered_image[mask2] = image[mask2]

        # filtered_image = image * np.expand_dims(mask, axis=-1).astype(np.uint8)
        # filtered_image = image * np.expand_dims(mask * mask2, axis=-1).astype(np.uint8)
        show(filtered_image, text="filtered image")
        hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
